var mongoose = require('mongoose');
var express = require('express');
const util = require('util');
const bodyParser = require('body-parser');
const app = express();
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/test');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {

});

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
};


app.use(allowCrossDomain);

var userSchema = mongoose.Schema({
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    }
});

var doctorSchema = mongoose.Schema({
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    address: {
        type: String,
        required: true
    },
    zipCode: {
        type: String,
        required: true
    }
});

var scheduleSchema = mongoose.Schema({
    _doctor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Doctor',
        required: true
    },
	mon: {
	    type: Boolean,
        default: true
    },
	tue: {
        type: Boolean,
        default: true
    },
	wed: {
        type: Boolean,
        default: true
    },
	thu: {
        type: Boolean,
        default: true
    },
	fri: {
        type: Boolean,
        default: true
    },
	sat: {
        type: Boolean,
        default: true
    },
	monStart: {
	    type: Date,
        default: '2015-03-25T08:30:00Z'
    },
	tueStart: {
        type: Date,
        default: '2015-03-25T08:30:00Z'
    },
	wedStart: {
        type: Date,
        default: '2015-03-25T08:30:00Z'
    },
	thuStart: {
        type: Date,
        default: '2015-03-25T08:30:00Z'
    },
	friStart: {
        type: Date,
        default: '2015-03-25T08:30:00Z'
    },
	satStart: {
        type: Date,
        default: '2015-03-25T08:30:00Z'
    },
	monEnd: {
        type: Date,
        default: '2015-03-25T18:30:00Z'
    },
	tueEnd: {
        type: Date,
        default: '2015-03-25T18:30:00Z'
    },
	wedEnd: {
        type: Date,
        default: '2015-03-25T18:30:00Z'
    },
	thuEnd: {
        type: Date,
        default: '2015-03-25T18:30:00Z'
    },
	friEnd: {
        type: Date,
        default: '2015-03-25T18:30:00Z'
    },
	satEnd: {
        type: Date,
        default: '2015-03-25T18:30:00Z'
    }
})

var appointmentSchema = mongoose.Schema({
    startTime: {
        type: Date,
        required: true
    },
    _user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: false
    },
    _doctor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Doctor',
        required: true
    }
})

var User = mongoose.model('User', userSchema);
var Doctor = mongoose.model('Doctor', doctorSchema);
var Appointment = mongoose.model('Appointment', appointmentSchema);
var WeeklySchedule = mongoose.model('WeeklySchedule', scheduleSchema);

app.post('/user/subscription', (req, res) => {
    var jsonObject = req.body;
    var firstname = jsonObject['firstname'];
    var lastname = jsonObject['lastname'];
    var email = jsonObject['email'];
    var password = jsonObject['password'];
    console.log(lastname);
    console.log(firstname);
    console.log(lastname);
    console.log(email);
    console.log(password);
    var newUser = new User({ firstname: firstname, lastname: lastname, email: email, password: password });
    newUser.save(function (err, room) {
        if (err) {
            res.writeHead(400, {'Content-type':'application/json'});
	        var result = {
                error: err
            };
            res.write(JSON.stringify(result));
            res.end();
        } else {
            res.writeHead(200, {'Content-Type': 'application/json'});
            var idResult = {
                id: room._id
            };
            res.write(JSON.stringify(idResult));
            res.end();
        }
    });
});

app.post('/user/login', (req, res) => {
    var jsonObject = req.body;
    var email = jsonObject['email'];
    var password = jsonObject['password'];
    var logUser = User.findOne({ email: email, password: password }, function(err, user) {
        console.log(user);
        if (err) {
            res.writeHead(500, {'Content-type': 'text/html'});
            res.end();
        } else {
            if (user) {
				res.writeHead(200, {'Content-type': 'application/json'});
                user._id.toHexString();
                res.write(JSON.stringify(user));
                res.end();
            } else {
				var logDoctor = Doctor.findOne({ email: email, password: password }, function(err, doctor) {
					console.log(doctor);
					if (err) {
						res.writeHead(500, {'Content-type': 'text/html'});
						res.end();
					}
					else {
						if (doctor) {
							res.writeHead(200, {'Content-type': 'application/json'});
							doctor._id.toHexString();
							res.write(JSON.stringify(doctor));
							res.end();
						} else {
							res.writeHead(401, {'Content-type': 'application/json'});
							var result = {
							 error: "invalid email/password"
							};
							res.write(JSON.stringify(result));
							res.end();
						}
					}
				});

            }
        }
    });
});

app.post('/user/info', (req, res) => {
    var jsonObject = req.body;
    var email = jsonObject['email'];
    var logUser = User.findOne({ email: email}, function(err, user) {
      console.log(user);
      if (err) {
        res.writeHead(500, {'Content-type': 'text/html'});
        res.end();
      }
      else {
        if (logUser) {
          res.writeHead(200, {'Content-type': 'application/json'});
          user._id.toHexString();
          res.write(JSON.stringify(user));
          res.end();
        } else {
          res.writeHead(401, {'Content-type': 'application/json'});
          var result = {
           error: "User not found"
          };
          res.write(JSON.stringify(result));
          res.end();
        }
      }
    });
});

app.post('/doctor/info', (req, res) => {
    var jsonObject = req.body;
    var email = jsonObject['email'];
    var logDoctor = Doctor.findOne({ email: email}, function(err, doctor) {
      console.log(doctor);
      if (err) {
        res.writeHead(500, {'Content-type': 'text/html'});
        res.end();
      }
      else {
        if (doctor) {
          res.writeHead(200, {'Content-type': 'application/json'});
          doctor._id.toHexString();
          res.write(JSON.stringify(doctor));
          res.end();
        } else {
          res.writeHead(401, {'Content-type': 'application/json'});
          var result = {
           error: "Doctor not found"
          };
          res.write(JSON.stringify(result));
          res.end();
        }
      }
    });
});

app.post('/doctor/subscription', (req, res) => {
    var jsonObject = req.body;
    var firstname = jsonObject['firstname'];
    var lastname = jsonObject['lastname'];
    var email = jsonObject['email'];
    var password = jsonObject['password'];
    var phone = jsonObject['phone'];
    var address = jsonObject['address'];
    var zipCode = jsonObject['zipCode'];
    console.log('==== DOC SUBSCRIPTION ====');
    var doctor = new Doctor({ firstname: firstname, lastname: lastname, email: email, password: password, phone: phone, address: address, zipCode: zipCode });
    doctor.save(function (err, room) {
        if (err) {
            console.log('== Doc Sub Error ==');
            console.log(err);
            res.writeHead(500, {'Content-type': 'text/html'});
            res.end();
        } else {
            res.writeHead(200, {'Content-Type': 'application/json'});
            var idResult = {
                id: room._id
            };
            let schedule = new WeeklySchedule({ _doctor: idResult.id });
            schedule.save(function (err, room) {  });
            res.write(JSON.stringify(idResult));
            res.end();
        }
    });
});

app.post('/doctor/all', (req, res) => {
    Doctor.find(function (err, all) {
        console.log(all);
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(JSON.stringify(all));
        res.end();
    });
    //let schedule = WeeklySchedule.findById({ _doctor:  mongoose.Types.ObjectId(docId)});

});

app.get('/appointment/:id', (req, res) => {
    let docId = req.params.id;

    Appointment.find({_doctor:  mongoose.Types.ObjectId(docId)}, function(err, apps) {
        var appMap = [];

        apps.forEach(function(app) {
            appMap.push(app);
        });

        console.log(appMap);
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(JSON.stringify(appMap));
        res.end();
    });
});

app.get('/user/:id', (req, res) => {
    let userId = req.params.id;

    User.findOne({_id:  mongoose.Types.ObjectId(userId)}, function(err, user) {
        console.log(user);
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(JSON.stringify(user));
        res.end();
    });
});

app.post('/appointment/bydate', (req, res) => {
    let jsonObject = req.body;
    let docId = jsonObject['doc'];
    let docObjID = mongoose.Types.ObjectId(docId);
    let date = jsonObject['date'];
    let myDate = new Date(date);
    let start = myDate.setHours(0,0,0,0);
    let end = myDate.setHours(23,59,59,999);
    console.log("Start / end date : ");
    console.log(start);
    console.log(end);
    Appointment.find({_doctor: docObjID, startTime: {"$gte": start, "$lt": end}}).populate('_user').exec(function (err, apps) {

        var appMap = [];
        apps.forEach(function (app) {
            appMap.push(app);
        });

        console.log("Appointments : ");
        console.log(appMap);

        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(JSON.stringify(appMap));
        res.end();
    });
});

app.post('/appointment/add', (req, res) => {
    let jsonObject = req.body;
    let docId = jsonObject['doc'];
    let docObjID = mongoose.Types.ObjectId(docId);
    let userId = jsonObject['user'];
    let userObjID = mongoose.Types.ObjectId(userId);
    let date = jsonObject['date'];
    console.log('doc : ' + docId + ' / user : ' + userId + ' / date : ' + date);
    let appointment = new Appointment({_doctor: docObjID, _user: userObjID, startTime: date});
    appointment.save(function (err, app) {
        if (err) {
            console.log('== Appointment Add Error ==');
            console.log(err);
            res.writeHead(500, {'Content-type': 'text/html'});
            res.end();
        } else {
            res.writeHead(200, {'Content-Type': 'application/json'});
            var idResult = {
                id: app._id
            };
            res.write(JSON.stringify(idResult));
            res.end();
        }
    });
});

app.post('/appointment/client', (req, res) => {
    let jsonObject = req.body;
    let userId = jsonObject['userId'];
    Appointment.find({_user:  mongoose.Types.ObjectId(userId)}).populate('_doctor').exec(function(err, apps) {
        var appMap = [];
        apps.forEach(function(app) {
            appMap.push(app);
        });
        console.log(appMap);
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(JSON.stringify(appMap));
        res.end();
    });
});

app.get('/schedule/:id', (req, res) => {
    let docId = req.params.id;
    let objectID = mongoose.Types.ObjectId(docId);
    WeeklySchedule.findOne({ _doctor:  mongoose.Types.ObjectId(docId)}, function (err, schedule) {
        console.log(schedule);
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(JSON.stringify(schedule));
        res.end();
    });
});

app.get('/appointment/del/:id', (req, res) => {
    let id = req.params.id;
    let objectID = mongoose.Types.ObjectId(id);
    Appointment.find({_id: objectID}).remove( function (err, del) {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(JSON.stringify(del));
        res.end();
    });
});

app.post('/schedule/updateTime', (req, res) => {
    let jsonObject = req.body;
    let docId = jsonObject['id'];
    let row = jsonObject['row'];
    let value = jsonObject['value'];
    let obj = {[row]: value};
	WeeklySchedule.findOne({ _doctor:  mongoose.Types.ObjectId(docId)}, function (err, schedule){
	    if(!schedule) {
	        console.log('Schedule not found');
            res.writeHead(404, {'Content-Type': 'text/html'});
            res.end();
        } else {
            console.log('Schedule found');
            console.log(obj);
	        schedule.update(obj, function(error, newSchedule) {
                res.writeHead(200, {'Content-Type': 'text/html'});
                let idResult = {
                    id: schedule._id
                };
                res.write(JSON.stringify(idResult));
                res.end();
            });
        }
    });
});

app.post('/schedule/add', (req, res) => {
    let jsonObject = req.body;
    var docId = jsonObject['id'];
    let objectID = mongoose.Types.ObjectId(docId);

});

app.post('/client/update', (req, res) => {
    let jsonObject = req.body;
    var email = jsonObject['email'];
    User.findOne({ email: email}, function (err, client) {
        console.log(client);
        client.firstname = jsonObject['firstname'];
        client.lastname = jsonObject['lastname'];
        client.save()

        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(JSON.stringify(client));
        res.end();
    });
});

app.post('/doctor/update', (req, res) => {
    let jsonObject = req.body;
    var email = jsonObject['email'];
    Doctor.findOne({ email: email}, function (err, doctor) {
        console.log(doctor);
        doctor.firstname = jsonObject['firstname'];
        doctor.lastname = jsonObject['lastname'];
        doctor.address = jsonObject['address'];
        doctor.zipCode = jsonObject['zipCode'];
        doctor.phone = jsonObject['phone'];
        doctor.save()

        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(JSON.stringify(doctor));
        res.end();

    });
});


app.listen(4242, () => {
});
