# EPIDOC

Website that will allow you to book appointments with Doctors

### Prerequisites

EPIDOC was written in REACT/REDUX and MongoDB was used as Database.


### Requirements

- MongoDB

- Yarn

- NodeJS

## Deployment

Start MongoDb at the root of your mangodb bin folder

```
>mongod
```

Start the back-end inside of the backend folder

```
>nodemon index.js
```

Start the project inside of the root

```
>yarn start
```

## Built With

* [material-ui] - React framework used
* [Yarn] - Dependency Management
* [REACT/REDUX] - Used to generate the interface

## Authors

* **Romain INNOCENT** - innoce_r
* **William VUU** - vuu_w
* **Alexis YANG** - yang_e