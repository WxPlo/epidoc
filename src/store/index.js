/**
 * Created by Straky on 02/06/2017.
 */
 import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
 import { reducer as reduxFormReducer } from 'redux-form';
 import thunk from 'redux-thunk';
 import login from '../reducers/login';
 import users from '../reducers/users';
 import date from '../reducers/dateReducer';
 import docList from '../reducers/data';
 import appointment from '../reducers/appointments';

 const reducer = combineReducers({
     form: reduxFormReducer, // mounted under "form"
     login,
     users,
     date,
     docList,
     appointment
 });
 const oldstore = (window.devToolsExtension
     ? window.devToolsExtension()(createStore)
     : createStore)(reducer);

 const store = createStore(
     reducer,
     compose(
         applyMiddleware(thunk),
         window.devToolsExtension ? window.devToolsExtension() : f => f
     )
 );

 export default store;
