/**
 * Created by Straky on 08/06/2017.
 */

const tableData = [
    { start: '6:00 am', end: '6:30 am', status: 'Open', valueStart: 6, valueEnd: 0},
    { start: '6:30 am', end: '7:00 am', status: 'Open', valueStart: 6, valueEnd: 30},
    { start: '7:00 am', end: '7:30 am', status: 'Open', valueStart: 7, valueEnd: 0},
    { start: '7:30 am', end: '8:00 am', status: 'Open', valueStart: 7, valueEnd: 30},
    { start: '8:00 am', end: '8:30 am', status: 'Open', valueStart: 8, valueEnd: 0},
    { start: '8:30 am', end: '9:00 am', status: 'Open', valueStart: 8, valueEnd: 30},
    { start: '9:00 am', end: '9:30 am', status: 'Open', valueStart: 9, valueEnd: 0},
    { start: '9:30 am', end: '10:00 am', status: 'Open', valueStart: 9, valueEnd: 30},
    { start: '10:00 am', end: '10:30 am', status: 'Open', valueStart: 10, valueEnd: 0},
    { start: '10:30 am', end: '11:00 am', status: 'Open', valueStart: 10, valueEnd: 30},
    { start: '11:00 am', end: '11:30 am', status: 'Open', valueStart: 11, valueEnd: 0},
    { start: '11:30 am', end: '12:00 pm', status: 'Open', valueStart: 11, valueEnd: 30},
    { start: '12:00 am', end: '12:30 pm', status: 'Open', valueStart: 12, valueEnd: 0},
    { start: '12:30 am', end: '1:00 pm', status: 'Open', valueStart: 12, valueEnd: 30},
    { start: '1:00 pm', end: '1:30 pm', status: 'Open', valueStart: 13, valueEnd: 0},
    { start: '1:30 pm', end: '2:00 pm', status: 'Open', valueStart: 13, valueEnd: 30},
    { start: '2:00 pm', end: '2:30 pm', status: 'Open', valueStart: 14, valueEnd: 0},
    { start: '2:30 pm', end: '3:00 pm', status: 'Open', valueStart: 14, valueEnd: 30},
    { start: '3:00 pm', end: '3:30 pm', status: 'Open', valueStart: 15, valueEnd: 0},
    { start: '3:30 pm', end: '4:00 pm', status: 'Open', valueStart: 15, valueEnd: 30},
    { start: '4:00 pm', end: '4:30 pm', status: 'Open', valueStart: 16, valueEnd: 0},
    { start: '4:30 pm', end: '5:00 pm', status: 'Open', valueStart: 16, valueEnd: 30},
    { start: '5:00 pm', end: '5:30 pm', status: 'Open', valueStart: 17, valueEnd: 0},
    { start: '5:30 pm', end: '6:00 pm', status: 'Open', valueStart: 17, valueEnd: 30},
    { start: '6:00 pm', end: '6:30 pm', status: 'Open', valueStart: 18, valueEnd: 0},
    { start: '6:30 pm', end: '7:00 pm', status: 'Open', valueStart: 18, valueEnd: 30},
    { start: '7:00 pm', end: '7:30 pm', status: 'Open', valueStart: 19, valueEnd: 0},
    { start: '7:30 pm', end: '8:00 pm', status: 'Open', valueStart: 19, valueEnd: 30},
    { start: '8:00 pm', end: '8:30 pm', status: 'Open', valueStart: 20, valueEnd: 0},
    { start: '8:30 pm', end: '9:00 pm', status: 'Open', valueStart: 20, valueEnd: 30},
    { start: '9:00 pm', end: '9:30 pm', status: 'Open', valueStart: 21, valueEnd: 0},
    { start: '9:30 pm', end: '10:00 pm', status: 'Open', valueStart: 21, valueEnd: 30},
    { start: '10:00 pm', end: '10:30 pm', status: 'Open', valueStart: 22, valueEnd: 0},
    { start: '10:30 pm', end: '11:00 pm', status: 'Open', valueStart: 22, valueEnd: 30},
    { start: '11:00 pm', end: '11:30 pm', status: 'Open', valueStart: 23, valueEnd: 0},
    { start: '11:30 pm', end: '12:00 am', status: 'Open', valueStart: 23, valueEnd: 30},
];

export default tableData;