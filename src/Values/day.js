/**
 * Created by Straky on 07/06/2017.
 */

export function getAccro(day) {
    switch (day) {
        case 'Monday':
            return 'mon';
        case 'Tuesday':
            return 'tue';
        case 'Wednesday':
            return 'wed';
        case 'Thursday':
            return 'thu';
        case 'Friday':
            return 'fri';
        case 'Saturday':
            return 'sat';
        case 'Sunday':
            return 'sun';
    }
}

export function getDayFromAccro(accro) {
    switch (accro) {
        case 'mon':
            return 'Monday';
        case 'tue':
            return 'Tuesday';
        case 'wed':
            return 'Wednesday';
        case 'thu':
            return 'Thursday';
        case 'fri':
            return 'Friday';
        case 'sat':
            return 'Saturday';
        case 'sun':
            return 'Sunday';
    }
}


export function getDay(number) {
    switch (number) {
        case 0:
            return 'sun';
        case 1:
            return 'mon';
        case 2:
            return 'tue';
        case 3:
            return 'wed';
        case 4:
            return 'thu';
        case 5:
            return 'fri';
        case 6:
            return 'sat';
    }
}