/**
 * Created by Straky on 05/06/2017.
 */
import isEmpty from 'lodash/isEmpty';

const initialState = {
    isLoggedIn: false,
    isDoctor: false,
    firstname: "",
    lastname: "",
    user: "",
    address: "",
    zipCode: "",
    phone: ""
};

export default (state = initialState, action = {}) => {
    switch(action.type) {
        case 'SET_CURRENT_USER':
            return {
                isLoggedIn: !isEmpty(action.user),
                isDoctor: !isEmpty(action.phone),
                firstname: action.firstname,
                lastname: action.lastname,
                user: action.user,
                address: action.address,
                zipCode: action.zipCode,
                phone: action.phone
            };
        default: return state;
    }
}
