import isEmpty from 'lodash/isEmpty';

type Appointment = {
  user: string,
  doctor: string,
  startTime: date,
    address: string,
    zip: string,
    id: string
};

let listApp: Array<Appointment> = [

]

export default (state = listApp, action = {}) => {
    switch(action.type) {
        case 'SET_APP_LIST':
            return [...state, {
              user: action.userId,
              doctor: action.docId,
              startTime: action.startTime,
               address: action.address,
                zip: action.zip,
                id: action.id
            }];
        case 'UNSET_APP_LIST':
            return [];
        default: return state;
    }
}
