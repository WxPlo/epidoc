import isEmpty from 'lodash/isEmpty';

type Doctor = {
  id: number,
  firstname: string,
  lastname: string,
  email: string,
  address: string,
  zipCode: string,
  phone: string,
  list: string,
  _id: string
};

let DOC_ID = 0;

let listDoc: Array<Doctor> = [

]


export default (state = listDoc, action = {}) => {
    switch(action.type) {
        case 'SET_DOC_LIST':
            return [...state, {
              id: ++DOC_ID,
              firstname: action.firstname,
              lastname: action.lastname,
              email: action.email,
              address: action.address,
              zipCode: action.zipCode,
              phone: action.phone,
              _id: action._id
            }];
        case 'UNSET_DOC_LIST':
            DOC_ID = 0;
            return [];
        default: return state;
    }
}
