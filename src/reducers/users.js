import isEmpty from 'lodash/isEmpty';

const initialState = {
    firstname: "",
    lastname: "",
    email: "",
    address: "",
    zipCode: "",
    phone: "",
};


export default (state = initialState, action = {}) => {
    switch(action.type) {
        case 'SET_CURRENT_INFO':
            console.log(action);
            if (!isEmpty(action))
                console.log('SETTING INFO');
            return {
              firstname: action.firstname,
              lastname: action.lastname,
              email: action.email,
              address: action.address,
              zipCode: action.zipCode,
              phone: action.phone,
            };
        default: return state;
    }
}
