/**
 * Created by Straky on 08/06/2017.
 */
const initialState = {
    date: ''
};

export default (state = initialState, action = {}) => {
    switch(action.type) {
        case 'SET_DATE':
            return {
                date: action.data
            };
        default: return state;
    }
}