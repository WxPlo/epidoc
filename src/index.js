// @flow
import React from 'react';
import { render } from 'react-dom';
import {App} from './components/Routing';
import { Provider } from 'react-redux';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import store from './store/index';
import { setCurrentUser } from './actions/signinActions';


if (localStorage.emailToken) {
    console.log('LOGGED IN');
    store.dispatch(setCurrentUser(localStorage.emailToken));
} else {
    console.log('NOT LOGGED IN');
}

render(
    <MuiThemeProvider>
        <Provider store={store}>
            <App/>
        </Provider>
    </MuiThemeProvider>,
    document.getElementById('root')
);
