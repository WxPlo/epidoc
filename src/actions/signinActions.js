/**
 * Created by Romain on 04/06/2017.
 */
import axios from 'axios'

export function setCurrentUser(user, phone, firstname, lastname, address, zipcode) {
    return {
        type: 'SET_CURRENT_USER',
        user: user,
        phone: phone,
        firstname: firstname,
        lastname: lastname,
        address: address,
        zipCode: zipcode,
    };
}

export function login(userData) {
    return dispatch => {
        const header = { headers: {'content-type': 'application/json'}};
        return axios.post('http://localhost:4242/user/login', userData, header).then(
            res  => {
                const token = res.data.email;
                const id = res.data._id;
                localStorage.setItem('emailToken', token);
                localStorage.setItem('idToken', id);
                if (res.data.phone) {
                    localStorage.setItem('typeToken', 'doc');
                    dispatch(setCurrentUser(token, res.data.phone, res.data.firstname,
                  res.data.lastname, res.data.address, res.data.zipCode));
                } else {
                    localStorage.setItem('typeToken', 'patient');
                    dispatch(setCurrentUser(token, "", res.data.firstname,
                  res.data.lastname, "", ""));
                }

            }
        );
    }
}
