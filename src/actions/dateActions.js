/**
 * Created by Straky on 08/06/2017.
 */
export function setRdv(rowId, date, docId) {
    return {
        type: 'SET_DATE',
        row: rowId,
        date: date,
        docId: docId
    };
}