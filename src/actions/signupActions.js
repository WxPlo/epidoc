/**
 * Created by Romain on 03/06/2017.
 */
import axios from 'axios'

export function userSignupRequest(userData) {
    return dispatch => {
        var header = { headers: {'content-type': 'application/json'}};
        if (userData.phone) {
            console.log('Got Phone');
            return axios.post('http://localhost:4242/doctor/subscription', userData, header);

        } else {
            console.log('Didnt get Phone');
            return axios.post('http://localhost:4242/user/subscription', userData, header);
        }
    }
}

export function addDoctorSchedule(idDoc) {
    return dispatch => {
        var header = { headers: {'content-type': 'application/json'}};
        return axios.post('http://localhost:4242/schedule/add', idDoc, header);
    }
}

export function doctorSignupRequest(userData) {
    return dispatch => {
        var header = { headers: {'content-type': 'application/json'}};
        return axios.post('http://localhost:4242/doctor/subscription', userData, header);
    }
}
