import axios from 'axios'

export function setCurrentList(firstname, lastname, email, address, zipCode, phone, _id) {
    return {
        type: 'SET_DOC_LIST',
        firstname: firstname,
        lastname: lastname,
        email: email,
        address: address,
        zipCode: zipCode,
        phone: phone,
        _id: _id
    };
}

export function unsetCurrentList( _id) {
    return {
        type: 'UNSET_DOC_LIST',
        _id: _id
    };
}


export function getAllDoc() {
    console.log("GETALL DOCS");
    return dispatch => {
        const header = { headers: {'content-type': 'application/json'}};
        return axios.post('http://localhost:4242/doctor/all', {}, header).then(
            res  => {
                console.log(res.data);
                dispatch(unsetCurrentList({}));
                for(var k in res.data) {
                    console.log('===== GETTING A DOCTOR =====');
                   console.log(k, res.data[k]._id);
                   dispatch(setCurrentList(res.data[k].firstname, res.data[k].lastname, res.data[k].email,
                      res.data[k].address, res.data[k].zipCode, res.data[k].phone, res.data[k]._id));
                }


            }
        );
    }
}
