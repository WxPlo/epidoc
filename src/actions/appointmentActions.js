import axios from 'axios';

export function setAppointment(userId, docId, date) {
    return dispatch => {
        const header = { headers: {'content-type': 'application/json'}};
        const data = {doc: docId, user: userId, date:date};
        return axios.post('http://localhost:4242/appointment/add', data, header);
    }
}

export function deleteAppointment(id) {
    return dispatch => {
        return axios.get('http://localhost:4242/appointment/del/' + id);
    }
}

export function setCurrentAppts(doc, user, startTime, address, zip, id) {
    return {
        type: 'SET_APP_LIST',
        docId: doc,
        userId: user,
        startTime: startTime,
        address: address,
        zip: zip,
        id: id
    };
}

export function unsetCurrentAppts(doc) {
    return {
        type: 'UNSET_APP_LIST',
        doc
    };
}

export function getAppointments(userId) {
    return dispatch => {
        const header = { headers: {'content-type': 'application/json'}};
        const data = { userId: userId };
        return axios.post('http://localhost:4242/appointment/client', data, header)
        .then(
            res  => {
              dispatch(unsetCurrentAppts({}));
              for(var k in res.data) {
                dispatch(setCurrentAppts(res.data[k]._doctor.firstname
                  , res.data[k]._user,
                   res.data[k].startTime, res.data[k]._doctor.address,
                    res.data[k]._doctor.zipCode, res.data[k]._id));
              }
            }
        );
    }
}
