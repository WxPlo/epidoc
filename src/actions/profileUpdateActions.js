import axios from 'axios'

export function setCurrentInfo(firstname, lastname, email, address, zipCode, phone) {
    return {
        type: 'SET_CURRENT_INFO',
        firstname: firstname,
        lastname: lastname,
        email: email,
        address: address,
        zipCode: zipCode,
        phone: phone
    };
}

export function getDoc(docData) {
    console.log("GETDOC");
    return dispatch => {
        const header = { headers: {'content-type': 'application/json'}};
        return axios.post('http://localhost:4242/doctor/info', docData, header).then(
            res  => {
                console.log(res.data);
                const token = res.data.email;
                console.log('Getting INFO of '.concat(token));
                dispatch(setCurrentInfo(res.data.firstname, res.data.lastname, res.data.email,
                   res.data.address, res.data.zipCode, res.data.phone));
            }
        );
    }
}

export function getUser(userData) {
  console.log("GETUSER");
    return dispatch => {
        const header = { headers: {'content-type': 'application/json'}};
        return axios.post('http://localhost:4242/user/info', userData, header).then(
            res  => {
                console.log(res.data);
                const token = res.data.email;
                console.log('Getting INFO of '.concat(token));
                dispatch(setCurrentInfo(res.data.firstname, res.data.lastname, res.data.email,
                   " ", " ", " "));
            }
        );
    }
}

export function updateDoc(docData) {
    console.log("UpdateDoc");
    return dispatch => {
        const header = { headers: {'content-type': 'application/json'}};
        return axios.post('http://localhost:4242/doctor/update', docData, header).then(
            res  => {
              console.log(res.data);
                dispatch(setCurrentInfo(res.data.firstname, res.data.lastname, res.data.email,
                   res.data.address, res.data.zipCode, res.data.phone));
            }
        );
    }
}

export function updateClient(clientData) {
    console.log("UpdateClient");
    return dispatch => {
        const header = { headers: {'content-type': 'application/json'}};
        return axios.post('http://localhost:4242/client/update', clientData, header).then(
            res  => {
              console.log(res.data);
                dispatch(setCurrentInfo(res.data.firstname, res.data.lastname, res.data.email,
                   " ", " ", " "));
            }
        );
    }
}
