/**
 * Created by Straky on 07/06/2017.
 */
import axios from 'axios'

export function setSchedule(data) {
    return {
        type: 'SET_SCHEDULE',
        data: data
    };
}

export function getSchedule(docId) {
    return dispatch => {
        const header = { headers: {'content-type': 'application/json'}};
        return axios.get('http://localhost:4242/schedule/' + docId, header);
    }
}

export function updateSchedule(docId, row, value) {
    return dispatch => {
        var header = { headers: {'content-type': 'application/json'}};
        let data = {
            "id": docId,
            "row": row,
            "value": value
        };
        return axios.post('http://localhost:4242/schedule/updateTime', data, header);
    }
}

