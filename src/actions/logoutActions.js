/**
 * Created by Straky on 05/06/2017.
 */

export function UnsetCurrentList() {
    return {
        type: 'UNSET_DOC_LIST',
        data: {}
    };
}

export function setCurrentUser(user) {
    return {
        type: 'SET_CURRENT_USER',
        user
    };
}

export function unsetAppts(user) {
    return {
        type: 'UNSET_APP_LIST',
        user
    };
}

export function logout() {
    return dispatch => {
        localStorage.removeItem('emailToken');
        localStorage.removeItem('idToken');
        localStorage.removeItem('typeToken');
        localStorage.removeItem('docRdvToken');
        dispatch(setCurrentUser({}));
        dispatch(UnsetCurrentList({}));
        dispatch(unsetAppts({}));
    }
}
