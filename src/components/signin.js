/**
 * Created by Romain on 04/06/2017.
 */
import React from 'react';
import SigninForm from './signinform';

class Login extends React.Component {

    render() {
        return (
            <div className="jumbotron" style={{backgroundColor:'#e3f2fd'}}>
                <h1>Sign in to start</h1>
                <p>Enter your E-mail address to log in.</p>
                <p>
                </p>
                    <SigninForm />
                <p></p>
            </div>
        );
    }
}

export default Login;
