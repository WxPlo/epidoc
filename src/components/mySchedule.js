/**
 * Created by Straky on 10/06/2017.
 */
import axios from 'axios';
import React from 'react';
import DatePicker from 'material-ui/DatePicker';
import { getDay } from '../Values/day'
import {
    Table,
    TableBody,
    TableFooter,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import day_table_row from '../Values/day_table_row';
import { getDayFromAccro } from '../Values/day';
import DailyMySchedule from "./dailyMySchedule";

const colorStyle = {
    colorGreen: {
        backgroundColor: 'green'
    },
    colorRed: {
        backgroundColor: 'LightPink'
    },
    colorWhite: {
        backgroundColor: 'white'
    },
};

class MySchedule extends React.Component {
    constructor(props) {
        super(props);

        let StartDate = new Date();
        this.state = {
            date: StartDate,
            day: getDay(StartDate.getDay()),
            table_data : day_table_row
        };

        this._onChange = this._onChange.bind(this);

    }

    _onChange(e, date) {
        let day = getDay(date.getDay());
        this.setState({
            date: date,
            day: day
        },
            () => {
                this.forceUpdate();
        });
    }

    disableSunday(date) {
        return date.getDay() === 0;
    }

    render() {
        return (
        <div>
            <DatePicker onChange={this._onChange}
                        hintText="Portrait Dialog"
                        mode="landscape"
                        defaultDate={this.state.date}
                        shouldDisableDate={this.disableSunday}/>
                <DailyMySchedule day={this.state.day} date={this.state.date.toString()}/>
        </div>
        );
    }
}

export default MySchedule;
