import React from 'react';
import Avatar from 'material-ui/Avatar';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import CommunicationChatBubble from 'material-ui/svg-icons/action/assignment';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { getAppointments } from '../actions/appointmentActions'
import Time from 'react-time-format'
import axios from 'axios';

class MyAppointments extends React.Component {
  constructor(props) {
    super(props);
      props.getAppointments(localStorage.idToken);
        this._onClick = this._onClick.bind(this);
  }

  _onClick(e) {
      //alert(e);
      console.log(e.target.value);
      e.preventDefault();
      axios.get('http://localhost:4242/appointment/del/' + e.target.value);
      this.props.getAppointments(localStorage.idToken);
  }

  render () {
    const itemL = (
      <div>

      {this.props.appt.map(p =>
        <span  key={p.startTime}>
        <ListItem value={p._id}
        primaryText={"Appointment with " + p.doctor}
        //secondaryText={"At "}
        //disabled={true}
        //onTouchTap={this._handleTouchTap.bind(this, p.id)}
        //rightIcon={<CommunicationChatBubble />}
        rightIcon={<button className="btn btn-danger" value={p.id} onClick={this._onClick}>X</button>}
        />
        <p><b>Date : <Time value={p.startTime} format="  YYYY/MM/DD at hh:mm" /> - </b>
            {p.address} {p.zip}
        </p>
        </span>
      )}
      </div>
    );

    return (
      <div>
      <span style={{fontSize:'35px'}}><b>Appointments</b></span>
        <List>
        <header></header>
          {itemL}
        </List>
      </div>
    );
  }
}

MyAppointments.propTypes = {
    appt: PropTypes.array.isRequired,
    getAppointments: PropTypes.func.isRequired
}

function mapStateToProps(state) {
    return {
        appt: state.appointment
    };
}

export default connect(mapStateToProps, {getAppointments})(MyAppointments);
