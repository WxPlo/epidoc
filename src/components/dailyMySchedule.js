/**
 * Created by Straky on 10/06/2017.
 */
import axios from 'axios';
import React from 'react';
import DatePicker from 'material-ui/DatePicker';
import { getDay } from '../Values/day'
import PropTypes from 'prop-types';
import {
    Table,
    TableBody,
    TableFooter,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import day_table_row from '../Values/day_table_row';
import { getDayFromAccro } from '../Values/day';

const colorStyle = {
    colorGreen: {
        backgroundColor: 'green'
    },
    colorRed: {
        backgroundColor: 'LightPink'
    },
    colorWhite: {
        backgroundColor: 'white'
    },
};

class DailyMySchedule extends React.Component {
    constructor(props) {
        super(props);

        let StartDate = new Date();
        this.state = {
            date: StartDate,
            day: getDay(StartDate.getDay()),
            table_data : day_table_row
        };
    }

    update  (data, day)  {
        const timeEqual = (hour, min, testHour, testMin) => {
            return hour === testHour && min === testMin;
        };

        const timeSup = (hour, min, testHour, testMin) => {
            if (testHour > hour) {
                return true;
            }
            else {
                return testHour === hour && testMin >= min;
            }
        };

        const timeInf = (hour, min, testHour, testMin) => {
            if (testHour < hour) {
                return true;
            }
            else return testHour === hour && testMin < min;
        };
        axios.get('http://localhost:4242/schedule/'+localStorage.idToken).then(
            res => {
                let newCalendar = JSON.parse(JSON.stringify(day_table_row));
                let open = res.data[day];
                if (!open) {
                    newCalendar.forEach(function (row) {
                        row.status = 'Closed';
                    });
                } else {
                    let startDate = new Date(res.data[day+'Start']);
                    let startHour = startDate.getUTCHours()+1;
                    let startMinute = startDate.getUTCMinutes();
                    let endDate = new Date(res.data[day+'End']);
                    let endHour = endDate.getUTCHours()+1;
                    let endMinute = endDate.getUTCMinutes();

                    data.forEach(function (obj) {
                        let date = new Date(obj.startTime);
                        var name = obj._user.firstname + ' ' + obj._user.lastname;

                        newCalendar.forEach(function (row) {
                            if (timeEqual(row.valueStart, row.valueEnd, date.getUTCHours(), date.getUTCMinutes())) {
                                row.status = name;
                            }
                        });
                    });
                    newCalendar.forEach(function (row) {
                        if (timeSup(endHour, endMinute, row.valueStart, row.valueEnd)) {
                            row.status = 'Closed';
                        }
                        if (timeInf(startHour, startMinute, row.valueStart, row.valueEnd)) {
                            row.status = 'Closed';
                        }
                    });
                }
                this.setState(
                    {
                        table_data: newCalendar
                    }
                )
            });
    };

    componentWillReceiveProps(nextProps, nextState) {
        const {date, day } = nextProps;
        const myData = {"doc": localStorage.idToken, "date": new Date(date)};
        const header = { headers: {'content-type': 'application/json'}};
        axios.post('http://localhost:4242/appointment/bydate', myData, header).then(
            res => {
                this.update(res.data, day);
            }
        );
    }

    componentDidMount() {
        const {date, day } = this.props;
        const myData = {"doc": localStorage.idToken, "date": new Date(date)};
        const header = { headers: {'content-type': 'application/json'}};
        axios.post('http://localhost:4242/appointment/bydate', myData, header).then(
            res => {
                this.update(res.data, day);
            }
        );
    }

    render() {
        const { day, date } = this.props;
        let fullDay = getDayFromAccro(day);
        return (
            <div>
                <h1>Daily Appointment - {fullDay}</h1>
                <Table
                    height='600px'
                    fixedHeader={true}
                    fixedFooter={false}
                    selectable={false}
                    multiSelectable={false}>
                    <TableHeader
                        enableSelectAll={false}>
                        <TableRow>
                            <TableHeaderColumn tooltip="scheduleStart">Start</TableHeaderColumn>
                            <TableHeaderColumn tooltip="scheduleEnd">End</TableHeaderColumn>
                            <TableHeaderColumn tooltip="status">Status</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody
                        deselectOnClickaway={true}
                        showRowHover={true}
                        stripedRows={false}>
                        {this.state.table_data.map( (row, index) => (
                            <TableRow key={index} style={
                                row.status !== 'Open'
                                    ? colorStyle.colorRed
                                    : colorStyle.colorWhite }
                                      selectable={row.status === 'Open'}>
                                <TableRowColumn>{row.start}</TableRowColumn>
                                <TableRowColumn>{row.end}</TableRowColumn>
                                <TableRowColumn>{row.status}</TableRowColumn>
                            </TableRow>
                        ))}
                    </TableBody>
                    <TableFooter adjustForCheckbox={this.state.showCheckboxes}>
                      
                    </TableFooter>
                </Table>
            </div>
        );
    }
}

DailyMySchedule.propTypes = {
    day: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired
};

export default DailyMySchedule;
