/**
 * Created by Straky on 02/06/2017.
 */
import React from 'react';
import {HashRouter as Router, Route, Switch} from 'react-router-dom';
import WeeklySchedule from './weeklySchedule'
import HomeNav from './Navbar';
import Guestpage from './guestPage';
import Signin from './signin';
import Tab from './Tab';
import Profile from './profileContainer';
import Appointment from './weeklyAppointment';
import Search from './search';
import MyAppointments from './myappts';
import MySchedule from './mySchedule'

class App extends React.Component {

    render(){
        return (
            <Router history={history}>
                <div>
                    <Route path="/" component={HomeNav}/>
                    <div className="container" id="container">
                    <Route exact path="/" component={Guestpage}/>
                    <Route exact path="/login" component={Signin}/>
                    <Route exact path="/signup" component={Tab}/>
                    <Route exact path="/schedule" component={WeeklySchedule}/>
                    <Route exact path="/profile" component={Profile}/>
                    <Route exact path="/search" component={Search}/>
                    <Route exact path="/bookappointment" component={Appointment}/>
                    <Route exact path="/myappts" component={MyAppointments}/>
                    <Route exact path="/myschedule" component={MySchedule}/>
                    </div>
              </div>
            </Router>
        );
    }
}
export  {App};
