import React from 'react';
import {Tabs, Tab} from 'material-ui/Tabs';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Signup from './signup';
import SignupDoc from './signupDoctor';
import {connect} from "react-redux";
import { userSignupRequest } from '../actions/signupActions'
import PropTypes from 'prop-types'

injectTapEventPlugin();

const styles = {
    headline: {
        fontSize: 24,
        paddingTop: 16,
        marginBottom: 12,
        fontWeight: 400,
    },
};

class myTab extends React.Component {

    render() {
        const { userSignupRequest } = this.props;
        return (
        <Tabs>
            <Tab label="patient">
                <div style={{backgroundColor: "#e3f2fd"}}>
                    <Signup userSignupRequest={userSignupRequest}/>
                </div>
            </Tab>
            <Tab label="doctor">
                <div style={{backgroundColor: "#e0e0e0"}}>
                    <SignupDoc userSignupRequest={userSignupRequest} />
                </div>
            </Tab>
        </Tabs> )
    };
}


myTab.propTypes = {
    userSignupRequest: PropTypes.func.isRequired
}

export default connect(null, { userSignupRequest })(myTab);
