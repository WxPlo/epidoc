import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { updateDoc } from '../actions/profileUpdateActions';

class DoctorProfile extends React.Component{
  constructor(props) {
      super(props);
      this.state = {
          firstname: this.props.users.firstname,
          lastname: this.props.users.lastname,
          email: this.props.users.email,
          address: this.props.users.address,
          zipCode: this.props.users.zipCode,
          phone:this.props.users.phone
      }
      this.onChange = this.onChange.bind(this);
      this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
      this.setState({ [e.target.name]: e.target.value});
  }

  onSubmit(e) {
      e.preventDefault();
      this.props.updateDoc(this.state);
  }

  render() {

    return (
      <div className="row">
      <div className="col-md-4" >
      <form onSubmit={this.onSubmit}>
            <label>Firstname</label><input type='text' name="firstname" onChange={this.onChange} className='form-control'  defaultValue={this.props.users.firstname}/>
            <label>Last name : </label><input type='text' name="lastname" onChange={this.onChange} className='form-control'  defaultValue={this.props.users.lastname}/>
            <label>Email : </label><input type='text' name="email" onChange={this.onChange} className='form-control'  defaultValue={this.props.users.email} disabled/>
            <label>Address : </label><input type='text' name="address" onChange={this.onChange} className='form-control'  defaultValue={this.props.users.address} />
            <label>ZipCode :</label><input type='text' name="zipCode" onChange={this.onChange} className='form-control'  defaultValue={this.props.users.zipCode} />
            <label>Phone number :</label><input type='text' name="phone" onChange={this.onChange} className='form-control'  defaultValue={this.props.users.phone} />

            <br/>

            <div className="form-group">
                <button className="btn btn-primary btn-lg">
                    Edit
                </button>
            </div>
      </form>
      </div>
      </div>
    );
  }
}


DoctorProfile.propTypes = {
    users: PropTypes.object.isRequired,
    updateDoc: PropTypes.func.isRequired
}

function mapStateToProps(state) {
    return {
        users: state.users
    };
}

export default connect(mapStateToProps, { updateDoc })(DoctorProfile);
