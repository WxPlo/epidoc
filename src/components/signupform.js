import React from 'react';
import sexe from '../Values/sexe'
import map from 'lodash/map'
import PropTypes from 'prop-types'
import validate from '../validation/signup'
import classnames from 'classnames'

class Signupform extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          firstname: '',
          lastname: '',
          email: '',
          password: '',
          error : {
              email: ''
          },
          passwordConfirm: '',
          sexe: ''
      }

      this.onChange = this.onChange.bind(this);
      this.onSubmit = this.onSubmit.bind(this);
  }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value});
    }

    onSubmit(e) {
        this.setState({ error : {}});
        const { error, isValid } = validate(this.state);
        e.preventDefault();
        if (isValid) {
            this.props.userSignupRequest(this.state).then(
                () => {
                    this.context.router.history.push('/login');
                }
            );
        } else {
            this.setState({ error : error});
        }
        console.log(this.state);
    }

    render() {
        const { error } = this.state;
        const options = map(sexe, (val, key) =>
            <option key={key} value={val}>{key}</option>
        );
        return (
            <form onSubmit={this.onSubmit}>
                <h1>Need a Doctor ?</h1><br/><br/>

                <div className={classnames("form-group", {'has-error' : error.firstname})}>
                    <label className="control-label">Firstname</label>
                    <input value={this.state.firstname} onChange={this.onChange} type="text" name="firstname" className="form-control"/>
                    {error.firstname && <span className="help-block">{error.firstname}</span>}
                </div>

                <div className={classnames("form-group", {'has-error' : error.lastname})}>
                    <label className="control-label">Lastname</label>
                    <input value={this.state.lastname} onChange={this.onChange} type="text" name="lastname" className="form-control"/>
                    {error.lastname && <span className="help-block">{error.lastname}</span>}
                </div>

                <div className={classnames("form-group", {'has-error' : error.email})}>
                    <label className="control-label">E-mail</label>
                    <input value={this.state.email} onChange={this.onChange} type="email" name="email" className="form-control"/>
                    {error.email && <span className="help-block">{error.email}</span>}
                </div>

                <div className={classnames("form-group", {'has-error' : error.password})}>
                    <label className="control-label">Password</label>
                    <input value={this.state.password} onChange={this.onChange} type="password" name="password" className="form-control"/>
                    {error.password && <span className="help-block">{error.password}</span>}
                </div>

                <div className={classnames("form-group", {'has-error' : error.passwordConfirm})}>
                    <label className="control-label">Confirm Password</label>
                    <input value={this.state.passwordConfirm} onChange={this.onChange} type="password" name="passwordConfirm" className="form-control"/>
                    {error.passwordConfirm && <span className="help-block">{error.passwordConfirm}</span>}
                </div>

                <div className={classnames("form-group", {'has-error' : error.sexe})}>
                    <label className="control-label">Gender</label>
                    <select
                        value={this.state.sexe}
                        onChange={this.onChange}
                        name="sexe"
                        className="form-control">
                        <option value="" disabled>Choose your gender</option>
                        {options}
                    </select>
                    {error.sexe && <span className="help-block">{error.sexe}</span>}
                </div>

                <div className="form-group">
                    <button className="btn btn-primary btn-lg">
                        Sign Up
                    </button>
                </div>
            </form>
        );
    }
}

Signupform.propTypes = {
    userSignupRequest: PropTypes.func.isRequired
}

Signupform.contextTypes = {
    router : PropTypes.object.isRequired
}

export default Signupform;
