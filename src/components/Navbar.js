import React from 'react';
import {Link} from "react-router-dom";
import { connect } from 'react-redux';
import { logout } from '../actions/logoutActions';
import PropTypes from 'prop-types'

class HomeNav extends React.Component {

    logout(e) {
        e.preventDefault();
        this.props.logout();
        this.context.router.history.push('/login');
    }

    render(){
        const { isLoggedIn } = this.props.logged;
        const { user } = this.props.logged;

        const schedule = (
          <li><Link to="/schedule">Schedule</Link></li>
        );

        const appointment = (
          <li><Link to="/search">Book Appointment</Link></li>
        );

        const myAppointment = (
            <li><Link to="/myappts">My Appointment</Link></li>
        );

        const mySchedule = (
            <li><Link to="/myschedule">My Schedule</Link></li>
        );

        const connected = (
            <ul className="nav navbar-nav navbar-right">
                <li><Link to="/profile">Profile</Link></li>
                { localStorage.typeToken === 'doc' ? schedule : appointment }
                { localStorage.typeToken === 'doc' ? mySchedule : myAppointment }
                <li><a href="#" onClick={this.logout.bind(this)}>Logout</a></li>
            </ul>
        );

        const guest = (
            <ul className="nav navbar-nav navbar-right">
                <li><Link to="/signup">Sign up</Link></li>
                <li><Link to="/login">Login</Link></li>
            </ul>
        );

        return (
            <div>
                <nav className="navbar navbar-light" style={{backgroundColor: '#e3f2fd'}}>
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <Link className="navbar-brand" to="/">EpiDoc</Link>
                        </div>
                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            { isLoggedIn ? connected : guest }
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
}

HomeNav.propTypes = {
    logged: PropTypes.object.isRequired,
    logout: PropTypes.func.isRequired
}

HomeNav.contextTypes = {
    router : PropTypes.object.isRequired
}

function mapStateToProps(state) {
    return {
        logged: state.login
    };
}

export default connect(mapStateToProps, { logout })(HomeNav);
