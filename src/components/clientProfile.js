import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { updateClient } from '../actions/profileUpdateActions';

class ClientProfile extends React.Component{
  constructor(props) {
      super(props);
      this.state = {
          firstname: this.props.users.firstname,
          lastname: this.props.users.lastname,
          email: this.props.users.email
      }

      this.onChange = this.onChange.bind(this);
      this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
      this.setState({ [e.target.name]: e.target.value});
  }

  onSubmit(e) {
      e.preventDefault();
      this.props.updateClient(this.state);
  }

  render() {

    return (

      <div className="row">
      <div className="col-md-4" >
      <form onSubmit={this.onSubmit}>
            <label>Firstname</label><input type='text' name="firstname" onChange={this.onChange} className='form-control'  defaultValue={this.props.users.firstname}/>
            <label>Last name : </label><input type='text' name="lastname" onChange={this.onChange} className='form-control'  defaultValue={this.props.users.lastname}/>
            <label>Email : </label><input type='text' name="email" onChange={this.onChange} className='form-control'  defaultValue={this.props.users.email} disabled/>

            <br/>

            <div className="form-group">
                <button className="btn btn-primary btn-lg">
                    Edit
                </button>
            </div>
      </form>
      </div>
      </div>
    );
  }
  }

ClientProfile.propTypes = {
    users: PropTypes.object.isRequired,
    updateClient: PropTypes.func.isRequired
}

function mapStateToProps(state) {
    return {
        users: state.users
    };
}


export default connect(mapStateToProps, { updateClient })(ClientProfile);
