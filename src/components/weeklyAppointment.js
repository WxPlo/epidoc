/**
 * Created by Straky on 06/06/2017.
 */
import React from 'react';
import PropTypes from 'prop-types'
import {connect} from "react-redux";
import DailyAppointment from './dailyAppointment'
import DatePicker from 'material-ui/DatePicker';
import { getDay } from '../Values/day'
import { setAppointment } from '../actions/appointmentActions';
import day_table_row from '../Values/day_table_row';

class weeklyAppointment extends React.Component {
    constructor() {
        super();
        this._onChange = this._onChange.bind(this);
        this._onClick = this._onClick.bind(this);
        let StartDate = new Date();
        this.state = {
            date: new Date(),
            start: new Date(),
            end: new Date(),
            day: getDay(StartDate.getDay())
        };
        localStorage.removeItem('rowToken');
        console.log('== Constructor ==')
    }

    _onChange(e, date) {
        let day = getDay(date.getDay());
        this.setState({
            date: date,
            day: day
        });
    }

    _onClick(e) {
        if (localStorage.rowToken) {
            let row = day_table_row[localStorage.rowToken];
            let copiedDate = this.state.date;
            if (this.state.date.getHours() === 0){
                //Cest chaud a expliquer mais en gros, ca prend le jour d'avant, donc on rajoute 24h pour rester sur le mm jour ....
                copiedDate.setUTCHours(row.valueStart + 24);
            } else {
                copiedDate.setUTCHours(row.valueStart);
            }
            copiedDate.setUTCMinutes(row.valueEnd);
            this.props.setAppointment(localStorage.idToken, localStorage.docRdvToken, copiedDate.toString())
                .then (
                    res => {
                        this.setState(this.state);
                    }
                );
        }
        e.preventDefault();
    }

    disableSunday(date) {
        return date.getDay() === 0;
    }

    render() {
        return (
            <div>
                <DatePicker onChange={this._onChange}
                            hintText="Portrait Dialog"
                            mode="landscape"
                            defaultDate={this.state.date}
                            shouldDisableDate={this.disableSunday}/>
                <button className="btn btn-primary" onClick={this._onClick}>Validate Appointment</button>
                <DailyAppointment day={this.state.day} date={this.state.date.toString()}/>
            </div>
        );
    };
}

weeklyAppointment.propTypes = {
    setAppointment: PropTypes.func.isRequired
}

export default connect(null, {setAppointment})(weeklyAppointment);