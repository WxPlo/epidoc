/**
 * Created by Romain on 04/06/2017.
 */
import React from 'react';
import PropTypes from 'prop-types'
import classnames from 'classnames'
import validate from '../validation/signin'
import { connect } from 'react-redux';
import { login } from '../actions/signinActions'
import { getDoc } from '../actions/profileUpdateActions'
import { getUser } from '../actions/profileUpdateActions'

class Signinform extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          email: '',
          password: '',
            error : {
                email: '',
                password: '',
                wrongId: ''
            }
        }
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value});
    }

    badLogin() {
        this.setState({error: { wrongId: 'Email or password incorrect.'}});
    }

    onSubmit(e) {
        this.setState({ error : {}});
        const { error, isValid } = validate(this.state);
        e.preventDefault();
        if (isValid) {
            this.props.login(this.state)
                .then(
                    (res) => {
                      const { isDoctor } = this.props.logged;
                      if (isDoctor)
                        this.props.getDoc(this.state);
                      else {
                        this.props.getUser(this.state);
                      }
                      this.context.router.history.push('/');
                    }
                )
                .catch(response => {
                    if (response.response.status === 401) {
                        this.setState({error: { wrongId: 'Email or password incorrect.'}});
                    }
            })
                ;
        } else {
            this.setState({ error : error});
        }
    }

    render() {
        const { error } = this.state;
        return (
            <form onSubmit={this.onSubmit}>

                { error.wrongId && <div className="alert alert-danger">{error.wrongId}</div>}

                <div className={classnames("form-group", {'has-error' : error.email})}>
                    <label className="control-label">E-mail</label>
                    <input value={this.state.email} onChange={this.onChange} type="email" name="email" className="form-control"/>
                    {error.email && <span className="help-block">{error.email}</span>}
                </div>

                <div className={classnames("form-group", {'has-error' : error.password})}>
                    <label className="control-label">Password</label>
                    <input value={this.state.password} onChange={this.onChange} type="password" name="password" className="form-control"/>
                    {error.password && <span className="help-block">{error.password}</span>}
                </div>

                <div className="form-group">
                    <button className="btn btn-primary btn-lg">
                        Sign In
                    </button>
                </div>
            </form>
        );
    }
}

Signinform.propTypes = {
    login: PropTypes.func.isRequired,
    getDoc: PropTypes.func.isRequired,
    getUser: PropTypes.func.isRequired,
    logged: PropTypes.object.isRequired,
}

function mapStateToProps(state) {
    return {
        logged: state.login
    };
}

Signinform.contextTypes = {
    router : PropTypes.object.isRequired
}


export default connect(mapStateToProps, { login, getDoc, getUser })(Signinform);
