import React from 'react';
import { connect } from 'react-redux';
import { Provider } from 'react-redux';
import ClientProfile from './clientProfile';
import DocProfile from './doctorProfile';
import PropTypes from 'prop-types'

class Profile extends React.Component {

    render() {
      const { isDoctor } = this.props.logged;
      const { user } = this.props.logged;

      const Client = (
        <div>
            <ClientProfile />
        </div>
      );

      const Doc = (
          <div>
              <DocProfile />
          </div>
      );

      return (

          <div className="jumbotron" style={{backgroundColor:'#e3f2fd'}}>
              <p style={{fontSize:'50px'}}><b>
                Profile</b>
              </p>
              { isDoctor ? Doc : Client }
          </div>
      );
    }
}

Profile.propTypes = {
    logged: PropTypes.object.isRequired,
}

function mapStateToProps(state) {
    return {
        logged: state.login
    };
}

export default connect(mapStateToProps)(Profile);
