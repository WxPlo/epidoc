/**
 * Created by Straky on 06/06/2017.
 */
import React from 'react';
import PropTypes from 'prop-types'
import Toggle from 'material-ui/Toggle';
import Paper from 'material-ui/Paper';
import MyTimePicker from './myTimePicker'
import {getAccro} from '../Values/day'
import { updateSchedule } from '../actions/scheduleActions'
import {connect} from "react-redux";

const styles = {
    block: {
        maxWidth: 275,
    },
    toggle: {
        marginBottom: 16,
    },
    thumbOff: {
        backgroundColor: '#ffcccc',
    },
    trackOff: {
        backgroundColor: '#ff9d9d',
    }
};

const stylePaper = {
    height: 200,
    width: 350,
    margin: 20,
    textAlign: 'center',
    display: 'inline-block',
};


class DailySchedule extends React.Component {

    constructor() {
        super();
        this.state = {};

        this.onToggle = this.onToggle.bind(this);
    }

    onToggle(e, value) {
        const { day } = this.props;
        this.props.updateSchedule(localStorage.idToken, getAccro(day), value);
    }

    render() {
        const {
            day,
            open,
            timeStart,
            timeEnd
        } = this.props;

        return (
            <Paper id="mon" style={stylePaper} zDepth={3}>
                <br/>
                <div style={styles.block}>
                    <Toggle label={ day }
                            onToggle={this.onToggle}
                            defaultToggled={open}
                            thumbStyle={styles.thumbOff}
                            trackStyle={styles.trackOff}/>
                </div>
                <br/>
                <MyTimePicker hint="Start" name={ day } time={timeStart}/>
                <br/>
                <MyTimePicker hint="End" name={ day } time={timeEnd}/>
            </Paper>
        );
    }
}

DailySchedule.propTypes = {
    day: PropTypes.string.isRequired,
    open: PropTypes.bool.isRequired,
    timeStart: PropTypes.string.isRequired,
    timeEnd: PropTypes.string.isRequired,
    updateSchedule: PropTypes.func.isRequired
};

export default connect(null, { updateSchedule })(DailySchedule);