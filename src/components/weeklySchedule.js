/**
 * Created by Straky on 06/06/2017.
 */
import React from 'react';
import DailySchedule from "./DailySchedule";
import { getSchedule } from '../actions/scheduleActions'
import PropTypes from 'prop-types'
import {connect} from "react-redux";

class weeklySchedule extends React.Component {
    constructor() {
        super();
        this._onChange = this._onChange.bind(this);
        this._onClick = this._onClick.bind(this);

        this.state = {
            schedule: {},
            mon: true,
            tue: true,
            wed: true,
            thu: true,
            fri: true,
            sat: true,
            monStart: Date.now().toString(),
            tueStart: Date.now().toString(),
            wedStart: Date.now().toString(),
            thuStart: Date.now().toString(),
            friStart: Date.now().toString(),
            satStart: Date.now().toString(),
            monEnd: Date.now().toString(),
            tueEnd: Date.now().toString(),
            wedEnd: Date.now().toString(),
            thuEnd: Date.now().toString(),
            friEnd: Date.now().toString(),
            satEnd: Date.now().toString()
        };
    }

    _onChange(e, time) {
        alert(time);
    }

    _onClick(e) {
        alert('Click');
        e.preventDefault();
        //this.props.getSchedule('5935f9497b5d85ba44fa8a55');
    }

    componentWillMount() {
        //this.setState ({ schedule: this.props.getSchedule('5935f9497b5d85ba44fa8a55')});
        console.log('UPDATING');
    }

    componentDidMount() {
        const update = (data) => {
            this.setState(
                {
                    mon: data.mon,
                    tue: data.tue,
                    wed: data.wed,
                    thu: data.thu,
                    fri: data.fri,
                    sat: data.sat,
                    monStart: data.monStart,
                    tueStart: data.tueStart,
                    wedStart: data.wedStart,
                    thuStart: data.thuStart,
                    friStart: data.friStart,
                    satStart: data.satStart,
                    monEnd: data.monEnd,
                    tueEnd: data.tueEnd,
                    wedEnd: data.wedEnd,
                    thuEnd: data.thuEnd,
                    friEnd: data.friEnd,
                    satEnd: data.satEnd
                }
            );
            console.log('UPDATED');
        }
        this.props.getSchedule(localStorage.idToken).then(
            res => {
            update(res.data);
            console.log('Schedule : ');
            console.log(res);
            }
        );
    }

    render() {
        return(
            <div>
                <h1>Weekly Schedule</h1>
                <div className="row">
                    <DailySchedule day="Monday"
                                   open={this.state.mon}
                                   timeStart={this.state.monStart}
                                   timeEnd={this.state.monEnd}/>
                    <DailySchedule day="Tuesday"
                                   open={this.state.tue}
                                   timeStart={this.state.tueStart}
                                   timeEnd={this.state.tueEnd}/>
                    <DailySchedule day="Wednesday"
                                   open={this.state.wed}
                                   timeStart={this.state.wedStart}
                                   timeEnd={this.state.wedEnd}/>
                    <DailySchedule day="Thursday"
                                   open={this.state.thu}
                                   timeStart={this.state.thuStart}
                                   timeEnd={this.state.thuEnd}/>
                    <DailySchedule day="Friday"
                                   open={this.state.fri}
                                   timeStart={this.state.friStart}
                                   timeEnd={this.state.friEnd}/>
                    <DailySchedule day="Saturday"
                                   open={this.state.sat}
                                   timeStart={this.state.satStart}
                                   timeEnd={this.state.satEnd}/>
                </div>
            </div>
        );
    }
}

weeklySchedule.propTypes = {
    getSchedule: PropTypes.func.isRequired
}

export default connect(null, { getSchedule })(weeklySchedule);