/**
 * Created by Straky on 06/06/2017.
 */
import React from 'react';
import PropTypes from 'prop-types'
import TimePicker from 'material-ui/TimePicker';
import {getAccro} from '../Values/day'
import { updateSchedule } from '../actions/scheduleActions'
import {connect} from "react-redux";

class myTimePicker extends React.Component {

    constructor() {
        super();
        this.state = {};

        this.onChange = this.onChange.bind(this);
    }

    onChange(event, date) {
        const { name, hint } = this.props;
        this.props.updateSchedule(localStorage.idToken, getAccro(name).concat(hint), date);
    }

    render() {
        const {
            name,
            hint,
            time
        } = this.props;
        let date = new Date(time);
        let hour = date.setHours(date.getHours());
        return (
            <TimePicker
                onChange={this.onChange}
                value={new Date(hour)}
                name={name}
                hintText={hint}/>
        );
    }
}

myTimePicker.propTypes = {
    name: PropTypes.string.isRequired,
    hint: PropTypes.string.isRequired,
    time: PropTypes.string.isRequired,
    updateSchedule: PropTypes.func.isRequired
};

export default connect(null, { updateSchedule })(myTimePicker);