import React from 'react';
import { connect } from 'react-redux';
import { getAllDoc } from '../actions/searchAction';
import PropTypes from 'prop-types';
import map from 'lodash/map';
import classnames from 'classnames';
import ListDocSearch from './ListMaterial';

class Search extends React.Component {
  constructor(props) {
      super(props);
      props.getAllDoc();
      this.state = {
          isSearching: false,
          lastname: '',
          zipCode: '',
          filter: []
      }
      this.onChange = this.onChange.bind(this);
      this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
      this.setState({ [e.target.name]: e.target.value});
  }

  onSubmit(e) {
      e.preventDefault();
      this.setState({ isSearching: true});

      console.log(this.state);
      console.log();
      const newArray = this.props.docList.filter(item =>
      item.lastname.indexOf(this.state.lastname) !== -1 && item.zipCode.indexOf(this.state.zipCode) !== -1);

      // const newArray = [];
      // if (this.state.zipCode && this.state.lastname)
      // {
      //   newArray = this.props.docList.filter(item =>
      //   item.lastname.indexOf(this.state.lastname) !== -1);
      //   console.log("BOTH");
      // }
      // else {
      //   this.state
      // }
      //
      // this.state.

      this.setState({
        filter: newArray
      })

      console.log(newArray);
      console.log(this.state.filter);

  };

  render() {
    const Doclist = (
      <div>
      <datalist id="myDocs">
          {this.props.docList.map(p => <option key={p.id} value={p.lastname}>{p.firstname}</option>)}
      </datalist>
      </div>
    );

    const Ziplist = (
      <div>
      <datalist id="myZips">
          {this.props.docList.map(p => <option key={p.id} value={p.zipCode}></option>)}
      </datalist>
      </div>
    );

    const { isDoctor } = this.props.logged;

    return (
      <div>
      <p style={{fontSize:'35px'}}><b>
        Find a doctor</b>
      </p>

      <form onSubmit={this.onSubmit}>
        <div className="row">
        		<div className="col-md-12">
        			<div className="row">
        				<div className="col-md-2">
                <input type="text" name="lastname" onChange={this.onChange} className="form-control" list="myDocs" placeholder="Name"/>

                  { isDoctor ? " " : Doclist }

        				</div>
            
        				<div className="col-md-2">
                <input type="text" name="zipCode" onChange={this.onChange} className="form-control" list="myZips" placeholder="Zip Code"/>

                  { isDoctor ? " " : Ziplist }

        				</div>

        				<div className="col-md-8">
                <div className="form-group">
                    <button className="btn btn-primary btn">
                        Search
                    </button>
                </div>
        				</div>
        			</div>
        		</div>
        	</div>
      </form>

     { this.state.isSearching ? <ListDocSearch filter={this.state.filter} /> : " "}

      </div>
    );
  }
}

Search.propTypes = {
    users: PropTypes.object.isRequired,
    docList: PropTypes.array.isRequired,
    getAllDoc: PropTypes.func.isRequired,
    logged: PropTypes.object.isRequired
}


function mapStateToProps(state) {
    return {
        users: state.users,
        docList: state.docList,
        logged: state.login
    };
}


export default connect(mapStateToProps, { getAllDoc })(Search);
