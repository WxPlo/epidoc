/**
 * Created by Straky on 06/06/2017.
 */
import React from 'react';
import sexe from '../Values/sexe'
import map from 'lodash/map'
import PropTypes from 'prop-types'
import validate from '../validation/signupDoctor'
import classnames from 'classnames'

class SignupformDoctor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstname: '',
            lastname: '',
            email: '',
            password: '',
            error : {
                email: ''
            },
            passwordConfirm: '',
            sexe: '',
            phone: '',
            zipCode: '',
            address: ''
        }

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value});
    }

    onSubmit(e) {
        this.setState({ error : {}});
        const { error, isValid } = validate(this.state);
        e.preventDefault();
        if (isValid) {
            this.props.userSignupRequest(this.state).then(
                (res) => {
                    console.log('Doc sub : ');
                    console.log(res);
                    this.context.router.history.push('/login');
                }
            );
        } else {
            this.setState({ error : error});
        }
        console.log(this.state);
    }

    render() {
        const { error } = this.state;
        const options = map(sexe, (val, key) =>
            <option key={key} value={val}>{key}</option>
        );
        return (
            <form onSubmit={this.onSubmit}>
                <h1>Are you a Doctor ?</h1><br/><br/>

                <div className={classnames("form-group", {'has-error' : error.firstname})}>
                    <label className="control-label">Firstname</label>
                    <input value={this.state.firstname} onChange={this.onChange} type="text" name="firstname" className="form-control"/>
                    {error.firstname && <span className="help-block">{error.firstname}</span>}
                </div>

                <div className={classnames("form-group", {'has-error' : error.lastname})}>
                    <label className="control-label">Lastname</label>
                    <input value={this.state.lastname} onChange={this.onChange} type="text" name="lastname" className="form-control"/>
                    {error.lastname && <span className="help-block">{error.lastname}</span>}
                </div>

                <div className={classnames("form-group", {'has-error' : error.email})}>
                    <label className="control-label">E-mail</label>
                    <input value={this.state.email} onChange={this.onChange} type="email" name="email" className="form-control"/>
                    {error.email && <span className="help-block">{error.email}</span>}
                </div>

                <div className={classnames("form-group", {'has-error' : error.password})}>
                    <label className="control-label">Password</label>
                    <input value={this.state.password} onChange={this.onChange} type="password" name="password" className="form-control"/>
                    {error.password && <span className="help-block">{error.password}</span>}
                </div>

                <div className={classnames("form-group", {'has-error' : error.passwordConfirm})}>
                    <label className="control-label">Confirm Password</label>
                    <input value={this.state.passwordConfirm} onChange={this.onChange} type="password" name="passwordConfirm" className="form-control"/>
                    {error.passwordConfirm && <span className="help-block">{error.passwordConfirm}</span>}
                </div>

                <div className={classnames("form-group", {'has-error' : error.phone})}>
                    <label className="control-label">Phone number</label>
                    <input value={this.state.phone} onChange={this.onChange} type="number" name="phone" className="form-control"/>
                    {error.phone && <span className="help-block">{error.phone}</span>}
                </div>

                <div className={classnames("form-group", {'has-error' : error.address})}>
                    <label className="control-label">Address</label>
                    <input value={this.state.address} onChange={this.onChange} type="text" name="address" className="form-control"/>
                    {error.address && <span className="help-block">{error.address}</span>}
                </div>

                <div className={classnames("form-group", {'has-error' : error.zipCode})}>
                    <label className="control-label">Zip code</label>
                    <input value={this.state.zipCode} onChange={this.onChange} type="number" name="zipCode" className="form-control"/>
                    {error.zipCode && <span className="help-block">{error.zipCode}</span>}
                </div>

                <div className={classnames("form-group", {'has-error' : error.sexe})}>
                    <label className="control-label">Gender</label>
                    <select
                        value={this.state.sexe}
                        onChange={this.onChange}
                        name="sexe"
                        className="form-control">
                        <option value="" disabled>Choose your gender</option>
                        {options}
                    </select>
                    {error.sexe && <span className="help-block">{error.sexe}</span>}
                </div>

                <div className="form-group">
                    <button className="btn btn-primary btn-lg">
                        Sign Up
                    </button>
                </div>
            </form>
        );
    }
}

SignupformDoctor.propTypes = {
    userSignupRequest: PropTypes.func.isRequired
}

SignupformDoctor.contextTypes = {
    router : PropTypes.object.isRequired
}

export default SignupformDoctor;
