/**
 * Created by Straky on 02/06/2017.
 */
import React from 'react'
import { connect } from 'react-redux';
import PropTypes from 'prop-types'
import Search from './search'

class Guest extends React.Component {

    render() {
        const { user } = this.props.logged;
        const { isLoggedIn } = this.props.logged;

        return (
          <section className="section" id="head">
<div className="container">

  <div className="row">
    <div className="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1 text-center">


      <h1 className="title"><b>EPIDOC</b></h1>
      <h2 className="subtitle">Get your appointment in no time !</h2>


      <h3 className="tagline">
        Potentially, the best place to find a doctor !<br/>
      </h3>

    </div>
  </div>

</div>
</section>
        );
    }
}

Guest.propTypes = {
    logged: PropTypes.object.isRequired
}

function mapStateToProps(state) {
    return {
        logged: state.login
    };
}

export default connect(mapStateToProps)(Guest);
