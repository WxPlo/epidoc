/**
 * Created by Romain on 03/06/2017.
 */
import React from 'react';
import SignupForm from './signupform';
import PropTypes from 'prop-types'

class Signup extends React.Component {

    render() {
        const { userSignupRequest } = this.props;
        return (
            <div className="row">
                <div className="col-md-4 col-md-offset-4">
                    <SignupForm userSignupRequest={userSignupRequest}/>
                </div>
            </div>
        );
    }
}

Signup.propTypes = {
    userSignupRequest: PropTypes.func.isRequired
}

export default Signup;