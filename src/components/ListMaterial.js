import React from 'react';
import Avatar from 'material-ui/Avatar';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import CommunicationChatBubble from 'material-ui/svg-icons/communication/chat-bubble';
import PropTypes from 'prop-types'
import { connect } from 'react-redux';

class ListDocSearch extends React.Component{

    _handleTouchTap(id) {
        localStorage.setItem('docRdvToken', this.props.docList[id-1]._id);
        this.context.router.history.push('/bookappointment');
    }

  render () {
    const itemL = (
      <div>
      {this.props.filter.map(p =>
        <ListItem key={p.id} value={p.id}
        primaryText={p.firstname + " " + p.lastname}
        secondaryText={"Address: " + p.address + " - Phone: " + p.phone +  " - ZipCode: " + p.zipCode}
        onTouchTap={this._handleTouchTap.bind(this, p.id)}
        rightIcon={<CommunicationChatBubble />}
        />
      )}
      </div>
    );

    return (
      <div>
        <List>
        <Subheader>Doctors</Subheader>
          {itemL }
        </List>
      </div>
    );
  }
}
ListDocSearch.propTypes = {
  docList: PropTypes.array.isRequired
}

ListDocSearch.contextTypes = {
    router : PropTypes.object.isRequired
}

function mapStateToProps(state) {
  return {
    docList: state.docList
  };
}

export default connect(mapStateToProps)(ListDocSearch);
