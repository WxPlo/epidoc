/**
 * Created by Straky on 05/06/2017.
 */
import React from 'react';
import PropTypes from 'prop-types'
import classnames from 'classnames'
import validate from '../validation/signin'

class Signinform extends React.Component {
    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onChange(e) {
        this.setState({ [e.target.name]: e.target.value});
    }

    onSubmit(e) {
        this.setState({ error : {}});
        const { error, isValid } = validate(this.state);
        e.preventDefault();
        if (isValid) {
            this.props.userSigninRequest(this.state)
                .then(
                    (res) => {
                        console.log('User data : ');
                        console.log(res.data.email);
                        document.cookie = "username=".concat(res.data.email);
                        this.context.router.history.push('/');
                    }
                    // ,
                    // (err) => {
                    //     console.log(err);
                    //     this.setState({error: { wrongId: 'Email or password incorrect.'}});
                    // }
                )
                .catch(response => {
                    if (response.response.status === 401) {
                        this.setState({error: { wrongId: 'Email or password incorrect.'}});
                    }
                    console.log(response.response.status);
                    // if (response) {
                    //
                    //     console.log(response.status);
                    //     console.log(response.headers);
                    //     this.badLogin();
                    // }
                })
            ;
        } else {
            this.setState({ error : error});
        }
        console.log(this.state);
    }

    render() {
        const { error } = this.state;
        return (
            <form onSubmit={this.onSubmit}>

                { error.wrongId && <div className="alert alert-danger">{error.wrongId}</div>}

                <div className={classnames("form-group", {'has-error' : error.email})}>
                    <label className="control-label">E-mail</label>
                    <input value={this.state.email} onChange={this.onChange} type="email" name="email" className="form-control"/>
                    {error.email && <span className="help-block">{error.email}</span>}
                </div>

                <div className={classnames("form-group", {'has-error' : error.password})}>
                    <label className="control-label">Password</label>
                    <input value={this.state.password} onChange={this.onChange} type="password" name="password" className="form-control"/>
                    {error.password && <span className="help-block">{error.password}</span>}
                </div>

                <div className="form-group">
                    <button className="btn btn-primary btn-lg">
                        Sign In
                    </button>
                </div>
            </form>
        );
    }
}

Signinform.contextTypes = {
    router : PropTypes.object.isRequired
}

export default Signinform;
