/**
 * Created by Straky on 07/06/2017.
 */
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
    Table,
    TableBody,
    TableFooter,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import day_table_row from '../Values/day_table_row';
import { getDayFromAccro } from '../Values/day';
import axios from 'axios';


const colorStyle = {
    colorGreen: {
        backgroundColor: 'green'
    },
    colorRed: {
        backgroundColor: 'LightPink'
    },
    colorWhite: {
        backgroundColor: 'white'
    },
};



class DailyAppointment extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fixedHeader: true,
            fixedFooter: false,
            stripedRows: false,
            showRowHover: false,
            selectable: true,
            multiSelectable: false,
            enableSelectAll: false,
            deselectOnClickaway: false,
            showCheckboxes: true,
            height: '600px',
            table_data : day_table_row
        };
        const {date, day} = this.props;
        console.log('Daily Constructor date : ');
        console.log(date);
        console.log('Daily Constructor day : ');
        console.log(day);
        this._onRowSelection = this._onRowSelection.bind(this);
    }

    _onRowSelection(row) {
        console.log(row);
        localStorage.setItem('rowToken', row);
    }



    update  (data, day)  {
        const timeEqual = (hour, min, testHour, testMin) => {
            return hour === testHour && min === testMin;
        };

        const timeSup = (hour, min, testHour, testMin) => {
            if (testHour > hour) {
                return true;
            }
            else {
                return testHour === hour && testMin >= min;
            }
        };

        const timeInf = (hour, min, testHour, testMin) => {
            if (testHour < hour) {
                return true;
            }
            else return testHour === hour && testMin < min;
        };
        axios.get('http://localhost:4242/schedule/'+localStorage.docRdvToken).then(
            res => {
                let newCalendar = JSON.parse(JSON.stringify(day_table_row));
                let open = res.data[day];
                if (!open) {
                    newCalendar.forEach(function (row) {
                        row.status = 'Closed';
                    });
                } else {
                    let startDate = new Date(res.data[day+'Start']);
                    let startHour = startDate.getUTCHours()+1;
                    let startMinute = startDate.getUTCMinutes();
                    let endDate = new Date(res.data[day+'End']);
                    let endHour = endDate.getUTCHours()+1;
                    let endMinute = endDate.getUTCMinutes();

                    data.forEach(function (obj) {
                        let date = new Date(obj.startTime);

                        newCalendar.forEach(function (row) {
                            if (timeEqual(row.valueStart, row.valueEnd, date.getUTCHours(), date.getUTCMinutes())) {
                                row.status = 'Reserved';
                            }
                        });
                    });
                    newCalendar.forEach(function (row) {
                        if (timeSup(endHour, endMinute, row.valueStart, row.valueEnd)) {
                            row.status = 'Closed';
                        }
                        if (timeInf(startHour, startMinute, row.valueStart, row.valueEnd)) {
                            row.status = 'Closed';
                        }
                    });
                }
                this.setState(
                    {
                        table_data: newCalendar
                    }
                )
            });
    };

    componentWillReceiveProps(nextProps, nextState) {
        const {date, day } = nextProps;
        const myData = {"doc": localStorage.docRdvToken, "date": new Date(date)};
        const header = { headers: {'content-type': 'application/json'}};
        axios.post('http://localhost:4242/appointment/bydate', myData, header).then(
            res => {
                this.update(res.data, day);
            }
        );
    }

    componentDidMount() {
        const {date, day } = this.props;
        const myData = {"doc": localStorage.docRdvToken, "date": new Date(date)};
        const header = { headers: {'content-type': 'application/json'}};
        axios.post('http://localhost:4242/appointment/bydate', myData, header).then(
            res => {
                this.update(res.data, day);
            }
        );
    }

    render() {
        const { day, date } = this.props;
        console.log('==== IN DAILY DATE : ====');
        console.log(date.toString());

        let fullDay = getDayFromAccro(day);
        return (
            <div>
                <h1>Daily Appointment - {fullDay}</h1>
                <Table
                    height={this.state.height}
                    fixedHeader={this.state.fixedHeader}
                    fixedFooter={this.state.fixedFooter}
                    selectable={this.state.selectable}
                    multiSelectable={this.state.multiSelectable}
                    onRowSelection={this._onRowSelection}>
                    <TableHeader
                        adjustForCheckbox={this.state.showCheckboxes}
                        enableSelectAll={this.state.enableSelectAll}>
                        <TableRow>
                            <TableHeaderColumn tooltip="scheduleStart">Start</TableHeaderColumn>
                            <TableHeaderColumn tooltip="scheduleEnd">End</TableHeaderColumn>
                            <TableHeaderColumn tooltip="status">Status</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody
                        displayRowCheckbox={this.state.showCheckboxes}
                        deselectOnClickaway={this.state.deselectOnClickaway}
                        showRowHover={this.state.showRowHover}
                        stripedRows={this.state.stripedRows}>
                        {this.state.table_data.map( (row, index) => (
                        <TableRow key={index} style={
                             row.status === 'Reserved' || row.status === 'Closed'
                                ? colorStyle.colorRed
                                : colorStyle.colorWhite }
                              selectable={row.status === 'Open'}>
                            <TableRowColumn>{row.start}</TableRowColumn>
                            <TableRowColumn>{row.end}</TableRowColumn>
                            <TableRowColumn>{row.status}</TableRowColumn>
                        </TableRow>
                    ))}
                    </TableBody>
                    <TableFooter adjustForCheckbox={this.state.showCheckboxes}>
                        
                    </TableFooter>
                </Table>
            </div>
        );
    };
}

DailyAppointment.propTypes = {
    day: PropTypes.string.isRequired,
    date: PropTypes.string.isRequired
};

export default connect()(DailyAppointment);
