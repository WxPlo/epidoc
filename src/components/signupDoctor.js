/**
 * Created by Straky on 06/06/2017.
 */
import React from 'react';
import SignupForm from './signupformDoctor';
import PropTypes from 'prop-types'

class SignupDoctor extends React.Component {

    render() {
        const { userSignupRequest } = this.props;
        return (
            <div className="row">
                <div className="col-md-4 col-md-offset-4">
                    <SignupForm userSignupRequest={userSignupRequest}/>
                </div>
            </div>
        );
    }
}

SignupDoctor.propTypes = {
    userSignupRequest: PropTypes.func.isRequired
}

export default SignupDoctor;
