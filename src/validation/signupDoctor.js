/**
 * Created by Straky on 06/06/2017.
 */
import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export default function validateInput(data) {
    let error = {};

    if (Validator.isEmpty(data.firstname)) {
        error.firstname = 'This field is required';
    }
    if (Validator.isEmpty(data.lastname)) {
        error.lastname = 'This field is required';
    }
    if (Validator.isEmpty(data.email)) {
        error.email = 'This field is required';
    }
    if (!Validator.isEmail(data.email)) {
        error.email = 'Email is invalid';
    }
    if (Validator.isEmpty(data.password)) {
        error.password = 'This field is required';
    }
    if (Validator.isEmpty(data.passwordConfirm)) {
        error.passwordConfirm = 'This field is required';
    }
    if (!Validator.equals(data.password, data.passwordConfirm)) {
        error.passwordConfirm = 'Passwords must match';
    }
    if (Validator.isEmpty(data.sexe)) {
        error.sexe = 'This field is required';
    }
    if (Validator.isEmpty(data.address)) {
        error.address = 'This field is required';
    }
    if (Validator.isEmpty(data.zipCode)) {
        error.zipCode = 'This field is required';
    }
    if (Validator.isEmpty(data.phone)) {
        error.phone = 'This field is required';
    }

    return {
        error,
        isValid: isEmpty(error)
    }
}