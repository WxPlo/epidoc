/**
 * Created by Romain on 04/06/2017.
 */
import Validator from 'validator';
import isEmpty from 'lodash/isEmpty';

export default function validateInput(data) {
    let error = {};

    if (Validator.isEmpty(data.email)) {
        error.email = 'This field is required';
    }
    if (!Validator.isEmail(data.email)) {
        error.email = 'Email is invalid';
    }
    if (Validator.isEmpty(data.password)) {
        error.password = 'This field is required';
    }

    return {
        error,
        isValid: isEmpty(error)
    }
}