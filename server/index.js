/**
 * Created by Straky on 03/06/2017.
 */
var express = require('express');
var app = express();
var path = require('path');

var webpack = require('webpack');
var webpackMiddleware = require('webpack-dev-middleware');
var webpackConfig = require('../config/webpack/build');

app.get('/*', (req, res) => {
    res.sendFile(path.join(__dirname, '../public/index.html'));
});

app.listen(3000, () => console.log('Running on localhost:3000'));